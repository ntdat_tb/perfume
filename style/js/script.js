$('.js-anchor-link').click(function(e){
    e.preventDefault();
    var target = $($(this).attr('href'));
    if(target.length){
      var scrollTo = target.offset().top;
      $('body, html').animate({scrollTop: scrollTo+'px'}, 800);
    }
  });
  $(window).scroll(function(){
    if ($(window).scrollTop() >= 500) {
        $('.menu').addClass('fixed-header');
        $('#logo-scoll').css({"display":"block"});
        $('#logo-no-scoll').css({"display":"none"});
        $('nav div').addClass('visible-title');
    }
    else {
        $('.menu').removeClass('fixed-header');
        $('#logo-scoll').css({"display":"none"});
        $('#logo-no-scoll').css({"display":"block"});
        $('nav div').removeClass('visible-title');
    }
    if ($(this).scrollTop() >= 50) {
        $('#return-to-top').fadeIn(200);
    } else {
        $('#return-to-top').fadeOut(200);
    }
});
$('#return-to-top').click(function() {
    $('body,html').animate({
        scrollTop : 0
    }, 500);
});